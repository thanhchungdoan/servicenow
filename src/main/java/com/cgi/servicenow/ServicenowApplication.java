package com.cgi.servicenow;

import com.cgi.servicenow.api.TicketDto;
import com.cgi.servicenow.service.impl.TicketService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class ServicenowApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicenowApplication.class, args);
    }
}
