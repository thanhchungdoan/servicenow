package com.cgi.servicenow.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;

public class TicketCreateDto {

    private Long idTicket;
    private String name;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private String ticketCloseDate;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private String creationDate;


    public TicketCreateDto() {
    }

    public Long getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(Long idTicket) {
        this.idTicket = idTicket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getTicketCloseDate() {
        return ticketCloseDate;
    }

    public void setTicketCloseDate(String ticketCloseDate) {
        this.ticketCloseDate = ticketCloseDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "TicketCreateDto{" +
                "idTicket=" + idTicket +
                ", name='" + name + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", ticketCloseDate='" + ticketCloseDate + '\'' +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }
}
