package com.cgi.servicenow.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;

@ApiModel(value = "TicketDto", description = "Information about ticket.")
public class TicketDto {

    @ApiModelProperty(value = "Ticket id.",example = "3")
    private Long idTicket;
    @ApiModelProperty(value = "Ticket name.",example = "Pavel")
    private String name;
    @ApiModelProperty(value = "Ticket id_person_creator.",example = "32")
    private Long idPersonCreator;
    @ApiModelProperty(value = "Ticket id_person_assigned.",example = "12")
    private Long idPersonAssigned;
        @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "Ticket close_date",example = "23-5-2014")
    private String ticketCloseDate;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "Ticket creation_date.",example = "14-2-2014")
    private String creationDate;


    public TicketDto() {
    }

    public Long getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(Long idTicket) {
        this.idTicket = idTicket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getTicketCloseDate() {
        return ticketCloseDate;
    }

    public void setTicketCloseDate(String ticketCloseDate) {
        this.ticketCloseDate = ticketCloseDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "TicketDto{" +
                "idTicket=" + idTicket +
                ", name='" + name + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", ticketCloseDate='" + ticketCloseDate + '\'' +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }
}
