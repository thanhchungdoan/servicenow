package com.cgi.servicenow.data.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "ticket")
public class Ticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ticket", nullable = false)
    private long idTicket;

    @Column(length = 50)
    private String name;

    @Column(name = "id_person_creator", nullable = false)
    private long idPersonCreator;

    @Column(name = "id_person_assigned", nullable = false)
    private long idPersonAssigned;

    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    @Column(name = "ticket_close_date")
    private String ticketCloseDate;

    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    @Column(name="creation_date")
    private String creationDate;

    public Ticket() {
    }

    public long getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(long idTicket) {
        this.idTicket = idTicket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getTicketCloseDate() {
        return ticketCloseDate;
    }

    public void setTicketCloseDate(String ticketCloseDate) {
        this.ticketCloseDate = ticketCloseDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "idTicket=" + idTicket +
                ", name='" + name + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", ticketCloseDate='" + ticketCloseDate + '\'' +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }
}
