package com.cgi.servicenow.data.repository;

import com.cgi.servicenow.data.entities.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.  Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket,Long> {

    @Override
    @EntityGraph(attributePaths = {"idTicket"})
    Page<Ticket> findAll(Pageable pageable);

    @Query("SELECT t FROM Ticket t WHERE t.idTicket = :id")
    Optional<Ticket> findById(@Param("id") Long id);

    @Query("SELECT t FROM Ticket t WHERE t.name = :name")
    List<Ticket> findByName(@Param("name") String name);

}
