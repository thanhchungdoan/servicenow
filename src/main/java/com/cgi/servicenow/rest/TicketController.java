package com.cgi.servicenow.rest;

import com.cgi.servicenow.api.TicketCreateDto;
import com.cgi.servicenow.api.TicketDto;
import com.cgi.servicenow.service.impl.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/tickets")
public class TicketController {

    private TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<TicketDto>> getAllTicketsPaginated(Pageable pageable) {
        return ResponseEntity.ok(ticketService.findAllTickets(pageable));
    }

    //  http://localhost:8094/servicenow/api/v1/tickets/id/2
    @GetMapping(path = "/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketDto> getTicketById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(ticketService.findTicketById(id));
    }

    // http://localhost:8094/servicenow/api/v1/tickets/name/Petr
    @GetMapping(path = "/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TicketDto>> getTicketByName(@PathVariable("name") String name) {
        return ResponseEntity.ok(ticketService.findTicketByName(name));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createTicket(@Valid @RequestBody TicketCreateDto ticketCreateDto) {
        ticketService.createTickets(ticketCreateDto);
    }
}
