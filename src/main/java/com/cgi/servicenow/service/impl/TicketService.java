package com.cgi.servicenow.service.impl;

import com.cgi.servicenow.api.TicketCreateDto;
import com.cgi.servicenow.api.TicketDto;
import com.cgi.servicenow.data.entities.Ticket;
import com.cgi.servicenow.data.repository.TicketRepository;
import com.cgi.servicenow.service.mapping.BeanMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Api(value="/tickets")
@Service
@Transactional
public class TicketService {

    private TicketRepository ticketRepository;
    private BeanMapping beanMapping;

    @Autowired
    public TicketService(TicketRepository ticketRepository, BeanMapping beanMapping) {
        this.ticketRepository = ticketRepository;
        this.beanMapping = beanMapping;
    }

    @ApiOperation(
            value = "Get all tickets pageable",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = TicketDto.class,
            responseContainer ="Page"
    )
    public Page<TicketDto> findAllTickets(Pageable pageable) {
        Page<Ticket> ticketPage = ticketRepository.findAll(pageable);
        return beanMapping.mapTo(ticketPage, TicketDto.class);
    }

    @ApiOperation(
            value = "Get tickets by id_ticket",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = TicketDto.class,
            responseContainer ="List"
    )
    public TicketDto findTicketById(Long id) {
        Ticket ticket = ticketRepository.findById(id).orElseThrow(()
                -> new EntityNotFoundException("Entity with id: " + id + " not found."));
        return beanMapping.mapTo(ticket,TicketDto.class);
    }

    @ApiOperation(
            value = "Get tickets by name",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = TicketDto.class,
            responseContainer ="List"
    )
    public List<TicketDto> findTicketByName(String name) {
        return beanMapping.mapTo(ticketRepository.findByName(name), TicketDto.class);
    }

    @ApiOperation(
            value = "Create tickets",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "PUT",
            response = Ticket.class
    )
    public void createTickets(TicketCreateDto ticketCreateDto) {
        Ticket ticket = beanMapping.mapTo(ticketCreateDto,Ticket.class);
        Ticket savedTicket = ticketRepository.saveAndFlush(ticket);
        ticketRepository.save(savedTicket);
    }

}
